![startScreen](startScreen.PNG)

## Project status
The project is as fare as I am concert complete following the end of the course, though there are a few things not working as expected, see [Known Bugs](#known-bugs) below. That said, I am currently working on a remake of the game that does not use Unity, see [Super Sub Battle Remake](https://gitlab.com/JSJCoding/super-sub-battle-remake).

## Description
Super Sub Battle is a 1v1 local multiplayer game made in 48 hours during the participation of a course by the Youth Science Association ([UNF](https://unf.dk/)) back in 2018. You play as two submarines and are trying to destroy each other. The gimmick is that both submarines are invisible while traversing the map. If you lose your bearings the submarines are equipped with sonars, these can be used to locate yourself and the enemy, but be careful the sonars also will reveal your location to the enemy.

## Known Bugs
- The timing at the between the submarine disappear under the surface and the player given control is off.
- Starting a game, the submarines might start below the surface.
- Sometimes when the submarines, sometimes the missals bounces off instead of hitting.


